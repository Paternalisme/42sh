/*
** double_redirection.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Mar  9 13:01:07 2014 barbis_j
** Last update Sun May 25 15:58:25 2014 barbis_j
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "mnsh.h"

int		d_redirection(const char *name)
{
  int		fd;

  fd = open(name, O_CREAT | O_WRONLY | O_APPEND,
	    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (fd == -1)
    write(1, "open error\n", 11);
  if (dup2(fd, 1) == -1)
    return (-1);
  return (1);
}

int		d_redirection_exe(char **cmd_arg, char ***env, int i)
{
  if (d_redirection(cmd_arg[i + 2]) == -1)
    return (-1);
  cmd_arg[i] = NULL;
  cmd_arg[i + 1] = NULL;
  command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 1, NULL);
  return (3);
}

int		d_is_redirection(char **cmd_arg)
{
  if (cmd_arg && cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    if (!my_strcmp(cmd_arg[0], ">") && !my_strcmp(cmd_arg[1], ">"))
      return (1);
  return (-1);
}

/*
** fork_cmdloop.c for ldfj in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Apr 24 15:09:43 2014 da-sil_l
** Last update Fri May 16 13:19:17 2014 da-sil_l
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include "mnsh.h"

static char	**put_signals(char **tab)
{
  tab[1] = my_strdup("Death of controlling process\n");
  tab[2] = my_strdup("Interrupted from keyboard\n");
  tab[3] = my_strdup("Quit from keyboard\n");
  tab[4] = my_strdup("");
  tab[5] = my_strdup("");
  tab[6] = my_strdup("Aborted\n");
  tab[7] = my_strdup("");
  tab[8] = my_strdup("Floating point exception\n");
  tab[9] = my_strdup("");
  tab[10] = my_strdup("");
  tab[11] = my_strdup("Segmentation fault\n");
  tab[12] = my_strdup("");
  tab[13] = my_strdup("Broken pipe\n");
  tab[14] = my_strdup("");
  tab[15] = my_strdup("");
  tab[16] = my_strdup("");
  tab[17] = my_strdup("Stopped\n");
  tab[18] = my_strdup("");
  tab[19] = my_strdup("Stopped\n");
  tab[20] = my_strdup("");
  tab[21] = my_strdup("");
  tab[22] = my_strdup("");
  tab[23] = my_strdup("Stopped\n");
  tab[24] = NULL;
  return (tab);
}

static int	error_status(int sig)
{
  char		**tab;
  int		i;

  if ((tab = malloc(25 * sizeof(char *))) == NULL)
    return (-1);
  tab = put_signals(tab);
  if (sig >= 0 && sig < 24)
    write(1, tab[sig], my_strlen(tab[sig]));
  return (0);
}

int	kill_verify(char **cmd_arg)
{
  if (cmd_arg[0] && find_kill(cmd_arg[0]) == 1)
    if (setpgrp() == -1)
      return (-1);
  return (0);
}

int	path_verify(char *name, char **path, int i)
{
  if (path[i] == NULL)
    {
      write(2, name, my_strlen(name));
      write(2, CMD_ERROR_MSG, my_strlen(CMD_ERROR_MSG));
      return (-1);
    }
  return (0);
}

int	my_wait(int flag, int res)
{
  int	status;

  status = 0;
  if (flag == 1 || flag == 2 || flag == 3)
    waitpid(-1, &status, WUNTRACED);
  if (WIFSIGNALED(status) || WIFEXITED(status))
    {
      if (WIFSIGNALED(status))
	error_status((res = WTERMSIG(status)));
      else
	res = WEXITSTATUS(status);
    }
  return (res);
}

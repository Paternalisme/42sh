/*
** path.c for zoubida in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Sun May 25 17:05:49 2014 da-sil_l
** Last update Sun May 25 17:07:24 2014 da-sil_l
*/

#include <stdlib.h>
#include "mnsh.h"

char		*path_finder(char **env)
{
  int		i;

  i = 0;
  while (env[i] != NULL)
    {
      if (my_strncmp("PATH=", env[i], 5) == 0)
	return (&env[i][5]);
      ++i;
    }
  return (NULL);
}

char		*make_pathname(char *path, char *cmd)
{
  char		*pathname;

  if (cmd)
    {
      if ((pathname = malloc((my_strlen(path) +
			       my_strlen(cmd) + 2) * sizeof(char))) == NULL)
	return (NULL);
      pathname = my_strcpy(pathname, path);
      pathname = my_strcat(pathname, "/");
      pathname = my_strcat(pathname, cmd);
      return (pathname);
    }
  return (path);
}

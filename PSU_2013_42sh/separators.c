/*
** separators.c for emflk in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Tue Apr 29 14:25:12 2014 da-sil_l
** Last update Sun May 25 15:39:34 2014 barbis_j
*/

#include <stdlib.h>
#include <unistd.h>
#include "mnsh.h"

int	is_ee_separator(char **cmd_arg)
{
  if (cmd_arg && cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    {
      if (my_strcmp(cmd_arg[0], "&") == 0 &&
	  my_strcmp(cmd_arg[1], "&") == 0)
	return (1);
    }
  return (-1);
}

int	is_oo_separator(char **cmd_arg)
{
  if (cmd_arg && cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    {
      if (!my_strcmp(cmd_arg[0], "|") && !my_strcmp(cmd_arg[1], "|"))
	return (1);
    }
  return (-1);
}

int	ee_separator_exe(char **cmd_arg, char ***env, int i)
{
  int	cmd_res;
  int	cmd;

  cmd_res = 0;
  cmd = 0;
  cmd_arg[i] = NULL;
  cmd_arg[i + 1] = NULL;
  if (cmd_arg[i - 1] != NULL)
    {
      cmd = command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 0, *env);
      cmd_res = my_wait(1, cmd_res);
    }
  if (cmd_res == 0 && cmd == 0)
    return (2);
  return (0);
}

int	oo_separator_exe(char **cmd_arg, char ***env, int i)
{
  int	cmd_res;
  int	cmd;

  cmd = 0;
  cmd_res = 0;
  cmd_arg[i] = NULL;
  cmd_arg[i + 1] = NULL;
  if (cmd_arg[i - 1] != NULL)
    {
      cmd = command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 0, *env);
      cmd_res = my_wait(1, cmd_res);
    }
  if (cmd_res == 0 && cmd == -1)
    return (2);
  return (0);
}

/*
** semi_colon.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Mar  9 15:12:53 2014 barbis_j
** Last update Sun May 25 15:02:41 2014 barbis_j
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "mnsh.h"

void		sc_loop(char **cmd_arg, char ***env, int i)
{
  int		j;
  int		ret;

  cmd_arg[i] = NULL;
  command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 1, NULL);
}

int		sc_exe(char **cmd_arg, char ***env, int i)
{
  sc_loop(cmd_arg, env, i);
  return (1);
}

int		is_sc(char **cmd_arg)
{
  if (cmd_arg)
    if (cmd_arg[0])
      if (my_strcmp(cmd_arg[0], ";") == 0)
	return (1);
  return (-1);
}

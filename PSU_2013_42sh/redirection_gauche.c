/*
** redirection_gauche.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar  7 14:00:10 2014 barbis_j
** Last update Sun May 25 14:54:48 2014 barbis_j
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "mnsh.h"

int		l_redirection(const char *name)
{
  int		fd;

  fd = open(name, O_RDONLY);
  if (fd == -1)
    write(1, OPEN_ERROR_MSG, my_strlen(OPEN_ERROR_MSG));
  if (dup2(fd, 0) == -1)
    return (-1);
  return (1);
}

int		l_redirection_exe(char **cmd_arg, char ***env, int i)
{
  if (l_redirection(cmd_arg[i + 1]) == -1)
    return (0);
  cmd_arg[i] = NULL;
  command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 1, NULL);
  return (1);
}

int		l_is_redirection(char **cmd_arg)
{
  if (cmd_arg && cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    if (!my_strcmp(cmd_arg[0], "<") && my_strcmp(cmd_arg[1], "<"))
      return (1);
  return (-1);
}

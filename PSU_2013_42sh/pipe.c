/*
** pipe.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Feb 24 16:01:03 2014 barbis_j
** Last update Sun May 25 14:51:20 2014 barbis_j
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "mnsh.h"

void		pipe_loop(char **cmd_arg, char ***env, int i)
{
  int		pipefd[2];

  pipe(pipefd);
  cmd_arg[i] = NULL;
  dup2(pipefd[1], 1);
  command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 0, NULL);
  close(pipefd[1]);
  dup2(pipefd[0], 0);
}

int		pipe_exe(char **cmd_arg, char ***env, int i)
{
  pipe_loop(cmd_arg, env, i);
  return (1);
}

int		is_pipe(char **cmd_arg)
{
  if (cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    {
      if (my_strcmp(cmd_arg[0], "|") == 0 &&
	  my_strcmp(cmd_arg[1], "|") != 0)
	return (1);
    }
  return (-1);
}

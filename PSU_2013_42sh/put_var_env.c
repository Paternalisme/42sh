/*
** put_var_env.c for mnsh2 in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh/builtins
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Tue May  6 16:59:27 2014 da-sil_l
** Last update Sun May 25 17:12:17 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include "mnsh.h"

static void	write_error_var(char *str)
{
  int		i;

  i = 0;
  while (str[i] && str[i] != '=')
    ++i;
  str[i] = 0;
  write(2, str, my_strlen(str));
  write(2, " ", 1);
  write(2, UNDEFINED_VAR, my_strlen(UNDEFINED_VAR));

}

static char	*fill_env_var(char *cmd, char **env, int pos)
{
  char		*var;
  int		i;

  i = 0;
  cmd = my_realloc(cmd, 2);
  cmd = my_strcat(cmd, "=");
  if (find_my(env, &cmd[pos + 1]) == NULL)
    {
      write_error_var(&cmd[pos + 1]);
      return (NULL);
    }
  if ((var = malloc((my_strlen(find_my(env, &cmd[pos + 1])) + 1)
		    * sizeof(char))) == NULL)
    return (NULL);
  var = find_my(env, &cmd[pos + 1]);
  cmd = my_realloc(cmd, my_strlen(var) + 2);
  while (var[i])
    {
      cmd[pos + i] = var[i];
      ++i;
    }
  cmd[pos + i] = 0;
  return (cmd);
}

char	**put_env_var(char **cmd_arg, char **env)
{
  int	i;
  int	j;

  i = 0;
  while (cmd_arg[i])
    {
      j = 0;
      while (cmd_arg[i] && cmd_arg[i][j])
	{
	  if (cmd_arg[i] && cmd_arg[i][j] == '$' && cmd_arg[i][j + 1])
	    if ((cmd_arg[i] = fill_env_var(cmd_arg[i], env, j)) == NULL)
	      return (NULL);
	  ++j;
	}
      ++i;
    }
  return (cmd_arg);
}

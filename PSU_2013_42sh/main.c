/*
** main.c for minishell1 in /home/barbis_j/Documents/Projets/PSU_2013_minishell1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Dec 13 16:52:11 2013 barbis_j
** Last update Sun May 25 15:25:04 2014 da-sil_l
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>
#include "mnsh.h"

static void	quit_handler(int sig)
{
  char		**tmp;

  tmp = NULL;
  write(1, "\n", 1);
  prompt(tmp, 0);
}

int	check_builtins(char **cmd_arg, char ***env)
{
  if (cmd_arg && my_exit(cmd_arg) == -1 && my_echo(cmd_arg) == -1 &&
      my_env(cmd_arg, env) != 1 && my_cd(cmd_arg, env) != 1 &&
      my_setenv(cmd_arg, env) != 1 && my_unsetenv(cmd_arg, *env) != 1)
    return (-1);
  return (1);
}

static int	init_sh(char *cmd, char ***env)
{
  char		**cmd_arg;

  if (cmd[0] != 0 && (cmd_arg = wordtab(cmd)) != NULL && cmd_arg[0] != NULL)
    {
      if (my_exit(cmd_arg) >= 0)
	return (my_exit(cmd_arg));
      if ((cmd_arg = put_env_var(cmd_arg, *env)) != NULL)
	command_exe(env, cmd_arg);
    }
  return (-1);
}

int		main(int ac, char **av, char **env)
{
  char		*cmd;
  int		ret;

  prompt(env, 1);
  ret = 0;
  if (signal(SIGINT, quit_handler) == SIG_ERR)
    return (-1);
  while ((cmd = get_next_line(0)) != NULL)
    {
      if ((ret = init_sh(cmd, &env)) >= 0)
	return (ret);
      free(cmd);
      prompt(env, 1);
    }
  return (1);
}

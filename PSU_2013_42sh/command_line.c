/*
** command_line.c for 42sh in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Sat May 17 18:31:10 2014 da-sil_l
** Last update Sun May 25 17:16:34 2014 da-sil_l
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include "mnsh.h"

int		command_abs(char **env, char **cmd_arg)
{
  int		i;

  i = 0;
  if (access(cmd_arg[0], F_OK) != 0)
    return (0);
  if (!vfork())
    {
      if (setpgrp() == -1)
	return (0);
      execve(cmd_arg[0], cmd_arg, env);
      exit(1);
    }
  my_wait(1, i);
  return (1);
}

static int	check_builtins_and_path(char **cmd_arg, char ***env,
					char **pathname)
{
  if (check_builtins(cmd_arg, env) == 1)
    return (1);
  if ((*pathname = path_finder(*env)) == NULL)
    return (1);
  return (0);
}

int		command_loop(char ***env, char **cmd_arg, int flag,
			     char **new_env)
{
  int		i;
  char		**path;
  char		*pathname;

  i = 0;
  if ((check_builtins_and_path(cmd_arg, env, &pathname)) == 1)
    return (0);
  path = sh_wordtab(pathname);
  while (path[i] != NULL &&
	 access((pathname = make_pathname(path[i], cmd_arg[0])), F_OK) != 0)
    ++i;
  if (path_verify(cmd_arg[0], path, i) == -1)
    return (-1);
  if (!vfork())
    {
      if (kill_verify(cmd_arg) == -1)
	return (0);
      if (flag == 2 || flag == 3)
      	execve(pathname, cmd_arg, new_env);
      else
	execve(pathname, cmd_arg, *env);
      exit(1);
    }
  my_wait(flag, i);
  return (0);
}

int		command_exe_loop(char **cmd_arg, int i, char ***env)
{
  int		j;
  execution	exec[9];
  function	funct[9];
  int		save1;
  int		ret;

  ret = 1;
  save1 = dup(1);
  init_exec(exec);
  init_funct(funct);
  j = 0;
  while (funct[j] && funct[j](&(cmd_arg[i])) <= 0)
    ++j;
  if (j < 8)
    ret = exec[j](cmd_arg, env, i);
  dup2(save1, 1);
  return (ret);
}

int		command_exe(char ***env, char **cmd_arg)
{
  int		ret;
  int		i;
  int		save0;

  save0 = dup(0);
  i = 0;
  while (cmd_arg[i])
    {
      ret = command_exe_loop(cmd_arg, i, env);
      if ((ret == 3 || ret == 2) && cmd_arg[i + 2])
	++i;
      ++i;
    }
  command_abs(*env, cmd_arg);
  if (ret == 2)
    command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i)], 1, NULL);
  if (ret == 1 || ret == 3)
    command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 1, NULL);
  dup2(save0, 0);
  return (1);
}

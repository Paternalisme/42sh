/*
** wordtab.c for wordtab in /home/barbis_j/Documents/Projets/wordtab
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Dec  9 14:02:38 2013 barbis_j
** Last update Fri May 16 20:12:52 2014 da-sil_l
*/

#include <stdlib.h>
#include "sh_wordtab.h"
#include "mnsh.h"

static int	sh_wordcount(char *str)
{
  if (str[0] == '\0')
    return (0);
  if (COND(str[0]))
    return (sh_wordcount(&str[1]));
  if (COND(str[1]) || str[1] == '\0')
    return (1 + sh_wordcount(&str[1]));
  return (sh_wordcount(&str[1]));
}

static int	sh_wordlength(char *str)
{
  if (str[0] == 0)
    return (0);
  if (COND(str[0]))
    return (0);
  return (1 + sh_wordlength(&str[1]));
}

static char	*sh_worddup(char *str, int length)
{
  char		*dest;

  if ((dest = malloc((length + 1) * sizeof(char))) == NULL)
    return (NULL);
  my_strncpy(dest, str, length);
  return (dest);
}

char	**sh_wordtab(char *str)
{
  char	**tab;
  int	i;
  int	m;
  int	len;

  i = 0;
  m = 0;
  if ((tab = malloc((sh_wordcount(str) + 1) * sizeof(char *))) == NULL)
    return (NULL);
  while (str[i] != 0)
    {
      if ((len = sh_wordlength(&str[i])) != 0)
	{
	  tab[m++] = sh_worddup(&str[i], len);
	  i = i + len;
	}
      else
	i = i + 1;
    }
  tab[m] = NULL;
  return (tab);
}

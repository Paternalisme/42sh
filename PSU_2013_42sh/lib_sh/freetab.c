/*
** freetab.c for freetab in /home/barbis_j/rendu/Piscine-C-lib/my
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Nov  5 16:43:30 2013 barbis_j
** Last update Thu May 15 20:49:54 2014 da-sil_l
*/

#include <stdlib.h>
#include "mnsh.h"

void	freetab(void **tab)
{
  int	nbr;

  nbr = sizetab(tab);
  while (nbr > 0)
    free(tab[--nbr]);
  free(tab);
}

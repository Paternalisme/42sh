/*
** utils2.c for ecrevisse in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh/lib_sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri May 16 13:21:45 2014 da-sil_l
** Last update Fri May 16 13:32:29 2014 da-sil_l
*/

#include "mnsh.h"

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  --n;
  i = 0;
  while (s1[i] == s2[i] && s1[i] && s2[i] && i != n)
    ++i;
  return (s1[i] - s2[i]);
}

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  if (n >= my_strlen(src))
    {
      while (src[i] != '\0')
	{
	  dest[i] = src[i];
	  i = i + 1;
	}
      dest[i] = '\0';
      return (dest);
    }
  while (n > 0)
    {
      dest[i] = src[i];
      i = i + 1;
      n = n - 1;
    }
  dest[i] = 0;
  return (dest);
}

int		find_kill(char *str)
{
  const char	kill[5] = "kill";
  int		i;
  int		j;

  i = 0;
  while (str[i])
    {
      j = 0;
      while (str[i] == kill[j])
	{
	  ++i;
	  ++j;
	  if (j == 4)
	    return (1);
	}
      ++i;
    }
  return (0);
}

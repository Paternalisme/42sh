/*
** utils.c for crabe in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh/lib_sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri May 16 13:19:53 2014 da-sil_l
** Last update Thu May 22 20:36:24 2014 Clément
*/

#include <stdlib.h>
#include <unistd.h>
#include "mnsh.h"

int	my_strlen(char *str)
{
  int	count;

  count = 0;
  while (str[count] != '\0')
    count = count + 1;
  return (count);
}

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (dest[i] != '\0')
    i = i + 1;
  while (src[j] != '\0')
    {
      dest[i + j] = src[j];
      j = j + 1;
    }
  dest[i + j] = '\0';
  return (dest);
}

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (s1 && s2)
    {
      while (s1[i] && s2[i] && s1[i] == s2[i])
	++i;
      return (s1[i] - s2[i]);
    }
  return (-1);
}

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}

char	*my_strdup(char *src)
{
  char	*dest;
  int	i;

  i = 0;
  if (src == NULL)
    return (NULL);
  dest = malloc((my_strlen(src) + 1) * sizeof(char));
  if (dest == NULL)
    {
      write(2, MALLOC_ERROR, my_strlen(MALLOC_ERROR));
      return (NULL);
    }
  while (src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = 0;
  return (dest);
}

/*
** wordtab.c for wordtab in /home/barbis_j/Documents/Projets/wordtab
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Dec  9 14:02:38 2013 barbis_j
** Last update Sat May 17 14:44:10 2014 da-sil_l
*/

#include <stdlib.h>
#include "mnsh.h"
#include "wordtab.h"

static int	wordcount(char *str)
{
  int		i;
  int		c;

  i = 0;
  c = 0;
  while (str[i])
    {
      while (str[i] && !KEEP(str[i]) && !KEEPTOO(str[i]) && !IGNORE(str[i]))
	{
	  if (!str[i + 1] || KEEP(str[i + 1]) ||
	      KEEPTOO(str[i + 1]) || IGNORE(str[i + 1]))
	    ++c;
	  ++i;
	}
      if (KEEP(str[i]) || KEEPTOO(str[i]))
	{
	  ++c;
	  ++i;
	}
      else if (IGNORE(str[i]))
	++i;
    }
  return (c);
}

static int	wordlength(char *str)
{
  int		i;

  i = 0;
  if (KEEP(str[i]) || KEEPTOO(str[i]))
    return (1);
  while (str[i] && !KEEP(str[i]) && !KEEPTOO(str[i]) && !IGNORE(str[i]))
    ++i;
  return (i);
}

static char	*worddup(char *str, int length)
{
  char		*dest;

  if ((dest = malloc((length + 1) * sizeof(char))) == NULL)
    return (NULL);
  my_strncpy(dest, str, length);
  return (dest);
}

char	**wordtab(char *str)
{
  char	**tab;
  int	i;
  int	m;
  int	len;
  int	total;

  i = 0;
  m = 0;
  total = wordcount(str);
  if ((tab = malloc((total + 1) * sizeof(char *))) == NULL)
    return (NULL);
  while (str[i] != 0 && total > 0)
    {
      if ((len = wordlength(&str[i])) != 0)
	{
	  tab[m++] = worddup(&str[i], len);
	  i = i + len;
	}
      else
	++i;
    }
  tab[m] = NULL;
  return (tab);
}

/*
** wordtab.h for wordtab in /home/barbis_j/Documents/Projets/wordtab
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Dec  9 14:10:23 2013 barbis_j
** Last update Sat May 24 18:09:19 2014 barbis_j
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

# define IGNORE(c) (((c) == ' ' || (c) == '\t') ? 1 : 0)
# define KEEP(c) (((c) == '|' || (c) == ';' || (c) == '<' || (c) == '>') ? 1 : 0)
# define KEEPTOO(c) (((c) == '&') ? 1 : 0)

#endif /* !WORDTAB_H_ */

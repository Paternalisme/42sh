/*
** get_next_line.c for get next line in /home/barbis_j/Documents/Projets/get_next_line
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Nov 19 15:50:14 2013 barbis_j
** Last update Wed Mar 19 13:22:59 2014 da-sil_l
*/

#include "get_next_line.h"

int	init_end(char *buff, int fd, int *ret)
{
  if (fd < 0)
    return (-1);
  *ret = read(fd, buff, BSIZE);
  if (*ret == -1 || *ret == 0)
    return (-1);
  return (1);
}

char		*my_srealloc(char *str, int n)
{
  char		*tmp;
  int		i;
  static int	size;

  i = 0;
  if (size == 0)
    size = n + n;
  size = size + n;
  tmp = malloc(size * 2);
  if (tmp == NULL)
    return (NULL);
  while (str[i] != 0)
    {
      tmp[i] = str[i];
      i = i + 1;
    }
  tmp[i] = '\0';
  free(str);
  return (tmp);
}

int	put_instr(t_line *line, char *buff, int *i, int *ret)
{
  while (*i < *ret)
    if (buff[*i] == 10)
      {
	(line->str)[line->j] = 0;
	(*i)++;
	return (1);
      }
    else
      (line->str)[(line->j)++] = buff[(*i)++];
  return (0);
}

int		gnl_loop(t_line *line, char *buff, int fd, int *ret)
{
  int		tmp;
  static int	i;

  tmp = i;
  if (put_instr(line, buff, &i, ret) == 1)
    return (2);
  line->str[line->j] = 0;
  *ret = read(fd, buff, BSIZE);
  if (*ret == -1 || (tmp == i && *ret == 0))
    {
      free(line->str);
      return (-1);
    }
  line->str = my_srealloc(line->str, BSIZE + 1);
  if (line->str == NULL)
    return (-1);
  i = 0;
  return (0);
}

char		*get_next_line(const int fd)
{
  static char	buff[BSIZE + 1];
  static int	ret;
  t_line	line;
  int		n;

  if (ret == 0)
    if (init_end(buff, fd, &ret) == -1)
      return (NULL);
  line.j = 0;
  if ((line.str = malloc(BSIZE * 2)) == NULL)
    return (NULL);
  while (ret != 0)
    {
      n = gnl_loop(&line, buff, fd, &ret);
      if (n == -1)
	return (NULL);
      else if (n == 2)
	return (line.str);
    }
  return (line.str);
}

/*
** sizetab.c for sizetab in /home/barbis_j/rendu/Piscine-C-lib/my
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Dec  4 16:18:23 2013 barbis_j
** Last update Wed Mar 19 13:34:48 2014 da-sil_l
*/

#include <stdlib.h>

int	sizetab(void **tab)
{
  int	c;

  c = 0;
  while (tab[c] != NULL)
    ++c;
  return (c);
}

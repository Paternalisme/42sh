/*
** my_strncpy.c for my_strncpy in /home/barbis_j/rendu/Piscine-C-Jour_06
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Mon Oct  7 09:48:59 2013 barbis_j
** Last update Fri May 16 17:33:39 2014 barbis_j
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (src[i] != 0 &&  n > 0)
    {
      dest[i] = src[i];
      ++i;
      --n;
    }
  dest[i] = 0;
  return (dest);
}

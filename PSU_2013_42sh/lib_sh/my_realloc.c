/*
** my_realloc.c for laitue in /home/da-sil_l/BitBucket/42sh/PSU_2013_42sh/lib_sh
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu May  8 17:12:32 2014 da-sil_l
** Last update Fri May 16 20:15:31 2014 da-sil_l
*/

#include <stdlib.h>
#include "mnsh.h"

char	*my_realloc(char *str, int size)
{
  int	i;
  char	*dest;

  i = 0;
  if ((dest = malloc((my_strlen(str) + size + 1) * sizeof(char))) == NULL)
    return (NULL);
  while (str[i])
    {
      dest[i] = str[i];
      ++i;
    }
  dest[i] = 0;
  return (dest);
}

/*
** get_next_line.h for get next line in /home/barbis_j/Documents/Projets/CPE_2013_getnextline
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Nov 22 14:31:06 2013 barbis_j
** Last update Thu May  8 17:18:47 2014 da-sil_l
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# include <unistd.h>
# include <stdlib.h>

# define BSIZE 15

typedef struct	s_line
{
  char		*str;
  int		j;
}		t_line;

int	put_instr(t_line *, char *, int *, int *);
int	init_end(char *, int, int *);
char	*get_next_line(const int);

#endif /* !GET_NEXT_LINE_H_ */

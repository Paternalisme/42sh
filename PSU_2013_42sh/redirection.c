/*
** redirection.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Mar  7 14:00:10 2014 barbis_j
** Last update Sun May 25 15:23:55 2014 barbis_j
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "mnsh.h"

int		find_last_cmd(char **cmd_arg, int i)
{
  if (i < 0)
    return (0);
  while (cmd_arg[i] && i > 0)
    --i;
  if (cmd_arg[i] == NULL && cmd_arg[i + 1])
    return (i + 1);
  else
    return (i);
}

int		redirection(const char *name)
{
  int		fd;

  fd = open(name, O_CREAT | O_TRUNC | O_WRONLY,
	    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (fd == -1)
    write(1, OPEN_ERROR_MSG, my_strlen(OPEN_ERROR_MSG));
  if (dup2(fd, 1) == -1)
    return (-1);
  return (1);
}

int		redirection_exe(char **cmd_arg, char ***env, int i)
{
  if (redirection(cmd_arg[i + 1]) == -1)
      return (0);
  cmd_arg[i] = NULL;
  command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 1, NULL);
  return (1);
}

int		is_redirection(char **cmd_arg)
{
  if (cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    if (my_strcmp(cmd_arg[0], ">") == 0 && my_strcmp(cmd_arg[1], ">") != 0)
      return (1);
  return (-1);
}

/*
** prompt.c for minishell1 in /home/barbis_j/Documents/Projets/PSU_2013_minishell1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Dec 13 16:55:02 2013 barbis_j
** Last update Sun May 25 17:19:21 2014 da-sil_l
*/

#include <unistd.h>
#include "mnsh.h"

void			prompt(char **env, int overwrite)
{
  static unsigned int	i = 1;
  static char		**save_env = NULL;
  char			*user;
  char			*host;

  if (overwrite == 1)
    save_env = env;
  if ((user = find_my(save_env, "USER=")) != NULL
      && (host = find_my(save_env, "HOSTNAME=")) != NULL)
    {
      write(1, "\033[31m", my_strlen("\033[31m"));
      write(1, "(", 1);
      write(1, user, my_strlen(user));
      write(1, "@", 1);
      write(1, host, my_strlen(host));
      write(1, " ", 1);
      my_putnbr(i);
      write(1, " 42sh)", 6);
      ++i;
      write(1, "\033[00m", my_strlen("\033[31m"));
    }
  else
    write(1, "\033[31m$\033[00m>", 13);
}

/*
** double_redirection_gauche.c for double_redirection_gauche in /home/bourge_h/Bitbucket/42sh/PSU_2013_42sh
** 
** Made by guillaume
** Login   <bourge_h@epitech.net>
** 
** Started on  Thu May 15 17:58:37 2014 guillaume
** Last update Sun May 25 16:27:08 2014 barbis_j
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "mnsh.h"

int		dg_redirection(char *name)
{
  char		*s;
  int		pipefd2[2];

  if ((pipe(pipefd2)) == -1)
    return (-1);
  write(2, ">", 1);
  while ((s = get_next_line(0)) != NULL)
    {
      if (my_strcmp(s, name) != 0)
	{
	  write(pipefd2[1], s, my_strlen(s));
	  write(pipefd2[1], "\n", 1);
	}
      else
	{
	  if ((close(pipefd2[1])) == -1 || (dup2(pipefd2[0], 0)) == -1)
	    return (-1);
	  return (1);
	}
      write(2, ">", 1);
      free(s);
    }
  if ((close(pipefd2[1])) == -1 || (dup2(pipefd2[0], 0)) == -1)
    return (-1);
  return (1);
}

int		dg_redirection_exe(char **cmd_arg, char ***env, int i)
{
  if (cmd_arg[i + 2] == NULL)
    {
      write(2, D_R_G_ERROR_MSG, my_strlen(D_R_G_ERROR_MSG));
      return (-1);
    }
  if (dg_redirection(cmd_arg[i + 2]) == -1)
    return (-1);
  cmd_arg[i] = NULL;
  cmd_arg[i + 1] = NULL;
  command_loop(env, &cmd_arg[find_last_cmd(cmd_arg, i - 1)], 1, NULL);
  return (3);
}

int		dg_is_redirection(char **cmd_arg)
{
  if (cmd_arg && cmd_arg[0] != NULL && cmd_arg[1] != NULL)
    {
      if (my_strcmp(cmd_arg[0], "<") == 0
	  && my_strcmp(cmd_arg[1], "<") == 0)
	return (1);
    }
  return (-1);
}

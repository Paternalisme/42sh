/*
** init_tab.c for init_tab in /home/dana_a/PSU_2013_42sh
** 
** Made by dana_a
** Login   <dana_a@epitech.net>
** 
** Started on  Mon May  5 13:52:03 2014 dana_a
** Last update Sat May 24 17:09:36 2014 barbis_j
*/

#include <stdlib.h>
#include "mnsh.h"

void		init_funct(function funct[9])
{
  funct[2] = &is_sc;
  funct[6] = &is_ee_separator;
  funct[7] = &is_oo_separator;
  funct[5] = &dg_is_redirection;
  funct[0] = &is_pipe;
  funct[1] = &is_redirection;
  funct[3] = &d_is_redirection;
  funct[4] = &l_is_redirection;
  funct[8] = NULL;
}

void		init_exec(execution exec[9])
{
  exec[2] = &sc_exe;
  exec[6] = &ee_separator_exe;
  exec[7] = &oo_separator_exe;
  exec[5] = &dg_redirection_exe;
  exec[0] = &pipe_exe;
  exec[1] = &redirection_exe;
  exec[3] = &d_redirection_exe;
  exec[4] = &l_redirection_exe;
  exec[8] = NULL;
}

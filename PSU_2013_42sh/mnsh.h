/*
** mnsh.h for minishell1 in /home/barbis_j/Documents/Projets/PSU_2013_minishell1
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Fri Dec 13 17:16:52 2013 barbis_j
** Last update Sun May 25 17:07:08 2014 da-sil_l
*/

#ifndef MNSH_H_
# define MNSH_H_

# define D_R_G_ERROR_MSG	"Missing name for redirect.\n"
# define CMD_ERROR_MSG		": Command not found\n"
# define OPEN_ERROR_MSG		"Open error\n"
# define ENV_U_MSG		"env: Option requires an argument\n"
# define EXIT_ERROR_MSG		"exit: Expression syntax\n"
# define NULL_CMD		"Invalid null command\n"
# define UNDEFINED_VAR		"Undefined variable\n"
# define CD_ERROR		": No such file or directory\n"
# define MALLOC_ERROR		"Memory allocation error\n"

typedef	int	(*function)(char **);
typedef	int	(*execution)(char **, char ***, int);

void		init_exec(execution *);
void		init_funct(function *);
void		prompt(char **, int);
void		freetab(void **);
void		my_putchar(char);
void		my_putstr(char *);
void		my_putnbr(int);
void		oldpwd_create(char ***);
char		*find_my(char**, char *);
char		*get_next_line(const int);
char		*path_finder(char **);
char		*make_pathname(char *, char *);
char		*my_strcpy(char *, char *);
char		*my_strcat(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_strdup(char *);
char		*my_realloc(char *, int);
char		**wordtab(char *);
char		**put_env_var(char **, char **);
char		**sh_wordtab(char *);
void		pipe_loop(char **, char ***, int);
int		find_kill(char *);
int		my_exit(char **);
int		is_ee_separator(char **);
int		is_oo_separator(char **);
int		ee_separator_exe(char **, char ***, int);
int		oo_separator_exe(char **, char ***, int);
int		command_loop(char ***, char **, int, char **);
int		my_echo(char **);
int		my_echo_putstr(char **, int);
int		my_strncmp(char *, char *, int);
int		cd_pwd_save(char *, char ***);
int		cd_old_to_pwd(char ***);
int		my_wait(int, int);
int		kill_verify(char **);
int		path_verify(char *, char **, int);
int		cd_pwd_to_oldpwd(char *, char ***);
int		my_strcmp(char *, char *);
int		sizetab(void **);
int		my_strlen(char *);
int		my_getnbr(char *);
int		copy_var_setenv(char *, char *, char ***);
int		is_sc(char **);
int		sc_exe(char **, char ***, int);
int		l_is_redirection(char **);
int		l_redirection_exe(char **, char ***, int);
int		d_is_redirection(char **);
int		d_redirection_exe(char **, char ***, int);
int		dg_redirection_exe(char **, char ***, int);
int		dg_is_redirection(char **);
int		check_builtins(char **, char ***);
int		is_pipe(char **);
int		pipe_exe(char **, char ***, int);
int		is_redirection(char **);
int		my_cd(char **, char ***);
int		redirection_exe(char **, char ***, int);
int		my_env(char **, char ***);
int		my_setenv(char **, char ***);
int		my_unsetenv(char **, char **);
int		command_exe(char ***, char **);
int		is_pipe(char **);
int		sh_pipe(int *, int *);
int		sh_setenv(char *, char *, char ***);
int		sh_unsetenv(char *, char **);
int		env_checkname(char *, char *);
int		find_last_cmd(char **, int);

#endif /* !MNSH_H_ */

/*
** my_exit.c for my_exit in /home/dana_a/Bitbucket/42sh/PSU_2013_42sh
** 
** Made by dana_a
** Login   <dana_a@epitech.net>
** 
** Started on  Mon May  5 16:50:28 2014 dana_a
** Last update Fri May 16 20:19:12 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include "mnsh.h"

int		my_exit(char **cmd_arg)
{
  unsigned char	ret;
  int		i;

  i = 0;
  if (my_strcmp(cmd_arg[0], "exit") == 0)
    {
      if (cmd_arg[1] == NULL)
        return (0);
      while (cmd_arg[1][++i] != 0)
        if (cmd_arg[1][i] > '9' || cmd_arg[1][i] < '0')
          {
            write(2, EXIT_ERROR_MSG, my_strlen(EXIT_ERROR_MSG));
            return (-1);
          }
      ret = (unsigned char)my_getnbr(cmd_arg[1]);
      return (ret);
    }
  return (-1);
}

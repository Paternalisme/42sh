/*
** my_putstr.c for my_putstr in /home/dana_a/Bitbucket/42sh/PSU_2013_42sh
** 
** Made by dana_a
** Login   <dana_a@epitech.net>
** 
** Started on  Fri May  2 14:44:47 2014 dana_a
** Last update Sun May 25 17:04:22 2014 da-sil_l
*/

#include <unistd.h>
#include "mnsh.h"

int	count_quote(char **tab)
{
  int	i;
  int	j;
  int	count;

  i = 0;
  count = 0;
  while (tab[i] != NULL)
    {
      j = 0;
      while (tab[i][j] != '\0')
	{
	  if (tab[i][j] == '"')
	    ++count;
	  ++j;
	}
      ++i;
    }
  if ((count = count % 2) > 0)
    {
      write(2, "Unmatched \".\n", 13);
      return (-1);
    }
  return (count);
}

char	*move_one_left(char *str, int n)
{
  while (str[n + 1])
    {
      str[n] = str[n + 1];
      ++n;
    }
  str[n - 1] = 0;
  return (str);
}

int	my_echo_putstr(char **tab, int len)
{
  int	i;
  int	j;

  i = 1;
  if (count_quote(tab) == -1)
    return (-1);
  while (i < len)
    {
      j = 0;
      while (tab[i][j] != '\0')
	{
	  if (tab[i][j + 1] != '\0' && tab[i][j] == '"')
	    tab[i] = move_one_left(tab[i], j);
	  else if (tab[i][j + 1] == '\0' && tab[i][j] == '"')
	    tab[i] = move_one_left(tab[i], j);
	  write(1, &tab[i][j], 1);
	  ++j;
	}
      if (i < len - 1)
	write(1, " ", 1);
      ++i;
    }
  return (1);
}

/*
** my_echo.c for my_echo in /home/dana_a/Bitbucket/42sh/PSU_2013_42sh
** 
** Made by dana_a
** Login   <dana_a@epitech.net>
** 
** Started on  Wed Apr 23 18:51:08 2014 dana_a
** Last update Sun May 25 16:07:22 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include "mnsh.h"

void		get_effect(char *str, int i)
{
  static char	table[11] = {'\\', 'a', 'b', 'c', 'e', 'f', 'n', 'r',
			      't', 'v', '\0'};
  static char	table_char[11] = {'\\', '\a', '\b', '\0', '\e', '\f', '\n',
				  '\r', '\t', '\v', '\0'};
  int		j;
  int		k;

  j = 0;
  k = 0;
  while (table[j] != '\0')
    {
      if (str[i + 1] == table[j])
	{
	  str[i] = table_char[j];
	  while (str[i + k + 1] != '\0')
	    {
	      str[i + k + 1] = str[i + k + 2];
	      ++k;
	    }
	}
      ++j;
    }
}

void		parcing_echo(char **tab, int count)
{
  int		i;
  int		j;

  i = 0;
  while (i < count)
    {
      j = 0;
      while (tab[i][j] != '\0')
	{
	  if (tab[i][j] == '\\')
	    get_effect(tab[i], j);
	  ++j;
	}
      ++i;
    }
}

int	my_echo(char **cmd)
{
  int	count;

  count = 1;
  if ((my_strcmp(cmd[0], "echo")) == 0)
    {
      while (cmd[count] != NULL && cmd[count][0] != ';' &&
	     cmd[count][0] != '<' && cmd[count][0] != '>' &&
	     cmd[count][0] != '|')
	++count;
      parcing_echo(cmd, count);
      if (my_echo_putstr(cmd, count) == -1)
	return (1);
      write(1, "\n", 1);
      return (1);
    }
  else
    return (-1);
}

/*
** mnsh_env.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat Mar  8 11:47:05 2014 barbis_j
** Last update Sun May 25 17:22:19 2014 da-sil_l
*/

#include <unistd.h>
#include <stdlib.h>
#include "mnsh.h"

int		my_setenv(char **cmd_arg, char ***env)
{
  if (my_strcmp(cmd_arg[0], "setenv") == 0)
    if (cmd_arg[1] == NULL)
      {
	cmd_arg[0] = "env";
	my_env(cmd_arg, env);
	return (1);
      }
    else
      {
	if (cmd_arg[2] != NULL)
	  {
	    if (cmd_arg[2] != NULL && cmd_arg[2][0] == '$')
	      copy_var_setenv(cmd_arg[1], cmd_arg[2], env);
	    else
	      sh_setenv(cmd_arg[1], cmd_arg[2], env);
	  }
	else
	  sh_setenv(cmd_arg[1], "", env);
	return (1);
      }
  return (-1);
}

int	copy_var_setenv(char *name, char *var, char ***env)
{
  char	*value;
  int	i;

  i = 0;
  while ((*env)[i] != NULL)
    {
      if (my_strncmp((*env)[i], &(var)[1], my_strlen(&(var)[1])) == 0 &&
	  (*env)[i][my_strlen(var) - 1] == '=')
	{
	  value = my_strdup(&((*env)[i])[my_strlen(var)]);
	  sh_setenv(name, value, env);
	  return (1);
	}
      ++i;
    }
  write(2, &(var)[1], my_strlen(&(var)[1]));
  write(2, ": Undefined variable\n", 21);
  return (-1);
}

char		**setenv_realloc(char **env, char *elem)
{
  char		**new_env;
  int		i;

  i = 0;
  if ((new_env = malloc((sizetab((void **)env) + 2) * sizeof(char *))) == NULL)
    return (NULL);
  while (env[i] != NULL)
    {
      new_env[i] = env[i];
      ++i;
    }
  new_env[i] = elem;
  new_env[i + 1] = NULL;
  return (new_env);
}

int		env_checkname(char *name, char *env_name)
{
  if (my_strncmp(env_name, name, my_strlen(name)) == 0 &&
      env_name[my_strlen(name)] == '=')
    return (1);
  return (-1);
}

int		sh_setenv(char *name, char *value, char ***env)
{
  int		i;
  char		*elem;

  i = 0;
  if ((elem = malloc(my_strlen(name) + my_strlen(value) + 2)) == NULL)
    return (1);
  elem = my_strcpy(elem, name);
  elem = my_strcat(elem, "=");
  if (value)
    elem = my_strcat(elem, value);
  while ((*env)[i] != NULL)
    {
      if (env_checkname(name, (*env)[i]) == 1)
	{
	  (*env)[i] = elem;
	  return (1);
	}
      ++i;
    }
  *env = setenv_realloc(*env, elem);
  return (1);
}

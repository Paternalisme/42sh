/*
** my_cd.c for dfko in /home/da-sil_l/rendu/PSU_2013_minishell1
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Wed Dec 18 15:37:52 2013 da-sil_l
** Last update Sat May 24 16:55:59 2014 Clément
*/

#include <unistd.h>
#include "mnsh.h"

static int	cd_go_home(char ***env)
{
  if (find_my(*env, "HOME="))
    {
      if (chdir(find_my(*env, "HOME=")) == -1)
	return (1);
    }
  else
    return (-1);
  cd_pwd_save(find_my(*env, "HOME="), env);
  return (1);
}

static void	cd_error(char *str)
{
  write(2, "cd: ", 4);
  write(2, str, my_strlen(str));
  write(2, CD_ERROR, my_strlen(CD_ERROR));
}

static int	cd_back(char ***env)
{
  if (find_my(*env, "OLDPWD="))
    {
      if (chdir(find_my(*env, "OLDPWD=")) == -1)
	{
	  write(2, "OLDPWD not set\n", 15);
	  return (-1);
	}
    }
  else
    {
      write(2, "OLDPWD not set\n", 15);
      return (-1);
    }
  write(1, find_my(*env, "OLDPWD="), my_strlen(find_my(*env, "OLDPWD=")));
  write(1, "\n", 1);
  cd_old_to_pwd(env);
  return (1);
}

int	my_cd(char **tab, char ***env)
{
  if (my_strcmp(tab[0], "cd") != 0)
    return (-1);
  if (*env && tab[1] == NULL)
    {
      cd_go_home(env);
      return (1);
    }
  else if (*env && !my_strcmp(tab[1], "-"))
    {
      cd_back(env);
      return (1);
    }
  if (chdir(tab[1]) == -1)
    cd_error(tab[1]);
  else if (tab && tab[1] && *env)
    cd_pwd_save(tab[1], env);
  return (1);
}

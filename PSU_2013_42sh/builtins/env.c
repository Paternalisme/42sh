/*
** mnsh_env.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sat Mar  8 13:45:43 2014 barbis_j
** Last update Sun May 25 17:11:20 2014 da-sil_l
*/

#include <unistd.h>
#include <stdlib.h>
#include "mnsh.h"

char		**my_cp_env(char **env)
{
  char		**save_env;
  int		i;

  i = 0;
  if ((save_env = malloc((sizetab((void **)env) + 1) * sizeof(char *))) == NULL)
    return (NULL);
  while (env[i] != NULL)
    {
      save_env[i] = my_strdup(env[i]);
      i++;
    }
  save_env[i] = NULL;
  return (save_env);
}

int		my_env_u(char **cmd_arg, char ***env)
{
  char		**save_env;
  static char	*new_cmd[2] = {"env", NULL};

  if (my_strcmp(cmd_arg[1], "-u") == 0 && cmd_arg[2] != NULL)
    {
      save_env = my_cp_env(*env);
      if (sh_unsetenv(cmd_arg[2], save_env) != 1)
	{
	  my_env(new_cmd, env);
	  return (-1);
	}
      else
	{
	  if (cmd_arg[3] == NULL)
	    my_env(new_cmd, &save_env);
	  else
	    command_loop(env, &cmd_arg[3], 3, save_env);
	}
    }
  else if (my_strcmp(cmd_arg[1], "-u") == 0 && cmd_arg[2] == NULL)
    write(2, ENV_U_MSG, my_strlen(ENV_U_MSG));
  freetab((void **)save_env);
  return (-1);
}

int		my_env_i(char **cmd_arg, char ***env)
{
  if (my_strcmp(cmd_arg[1], "-i") == 0 && cmd_arg[2] != NULL)
    {
      command_loop(env, &cmd_arg[2], 2, NULL);
      return (-1);
    }
  else if (my_strcmp(cmd_arg[1], "-i") == 0 && cmd_arg[2] == NULL)
    return (-1);
  return (1);
 }

int		my_env(char **cmd_arg, char ***env)
{
  int		i;

  i = 0;
  if (my_strcmp(cmd_arg[0], "env") == 0 && cmd_arg[1] == NULL)
    {
      while ((*env)[i] != NULL)
	{
	  write(1, (*env)[i], my_strlen((*env)[i]));
	  write(1, "\n", 1);
	  ++i;
	}
      return (1);
    }
  else if (my_strcmp(cmd_arg[0], "env") == 0 && cmd_arg[1] != NULL)
    {
      if (my_env_i(cmd_arg, env) == -1)
	return (1);
      if (my_env_u(cmd_arg, env) == -1)
	return (1);
    }
  return (-1);
}

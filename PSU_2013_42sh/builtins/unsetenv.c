/*
** mnsh_unsetenv.c for mnsh2 in /home/barbis_j/Documents/Projets/PSU_2013_minishell2
** 
** Made by barbis_j
** Login   <barbis_j@epitech.net>
** 
** Started on  Sun Mar  9 16:02:33 2014 barbis_j
** Last update Sat Apr 26 13:25:51 2014 da-sil_l
*/

#include <unistd.h>
#include <stdlib.h>
#include "mnsh.h"

int		my_unsetenv(char **cmd_arg, char **env)
{
  if (my_strcmp(cmd_arg[0], "unsetenv") == 0)
    if (cmd_arg[1] == NULL)
      write(2, "Usage : unsetenv NAME\n", 20);
    else
      {
	sh_unsetenv(cmd_arg[1], env);
	return (1);
      }
  return (-1);
}

int		sh_unsetenv(char *name, char **env)
{
  int		i;

  i = 0;
  while (env[i] != NULL && env_checkname(name, env[i]) != 1)
    ++i;
  if (env[i] == NULL)
    return (-1);
  while (env[i + 1] != NULL)
    {
      env[i] = env[i + 1];
      ++i;
    }
  env[i] = NULL;
  return (1);
}

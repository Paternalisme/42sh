/*
** cd_oldpwd.c for 42sh in /home/da-sil_l/rendu/PSU_2013_minishell2/test
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Sat Mar 15 12:32:15 2014 da-sil_l
** Last update Sun May 25 17:21:07 2014 da-sil_l
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "mnsh.h"

char	*find_my(char **env, char *str)
{
  int	i;

  i = 0;
  while (env[i] != NULL && my_strncmp(env[i], str, my_strlen(str)) != 0)
    ++i;
  if (env[i] == NULL)
    return (NULL);
  return (&env[i][my_strlen(str)]);
}

static int	cd_pwd_trunc(char ***env)
{
  int		i;
  int		j;

  i = 0;
  j = 4;
  while ((*env)[i] != NULL && my_strncmp((*env)[i], "PWD=", 4) != 0)
    ++i;
  if ((*env)[i] == NULL)
    return (-1);
  while ((*env)[i][j] != 0)
    ++j;
  while ((*env)[i][j] != '/')
    --j;
  (*env)[i][j] = 0;
  return (1);
}

char	*cd_pwd_add(char *line, char **env)
{
  char	*new_pwd;
  char	*pwd;
  int	i;
  int	j;

  j = 0;
  if ((pwd = my_strdup(find_my(env, "PWD="))) == NULL)
    return (NULL);
  if ((new_pwd = malloc((my_strlen(pwd) +
			 my_strlen(line) + 2) * sizeof(char))) == NULL)
    return (NULL);
  new_pwd = memset(new_pwd, 0, sizeof(char));
  if (pwd)
    new_pwd = my_strcat(new_pwd, pwd);
  new_pwd = my_strcat(new_pwd, "/");
  i = my_strlen(new_pwd);
  while (line[j])
    new_pwd[i++] = line[j++];
  new_pwd[i] = 0;
  return (new_pwd);
}

static int	cd_pwd_update(char *line, char ***env)
{
  char		**tab;

  if ((tab = malloc(5 * sizeof(char *))) == NULL)
    return (-1);
  tab[0] = my_strdup("setenv");
  tab[1] = my_strdup("PWD");
  if (line[0] == '/')
    tab[2] = my_strdup(line);
  else
    if ((tab[2] = my_strdup(cd_pwd_add(line, *env))) == NULL)
      return (-1);
  tab[3] = my_strdup("1");
  tab[4] = NULL;
  if (tab[2] != NULL)
    my_setenv(tab, env);
  return (1);
}

int	cd_pwd_save(char *line, char ***env)
{
  cd_pwd_to_oldpwd(line, env);
  if (my_strcmp(line, "..") == 0)
    cd_pwd_trunc(env);
  else
    cd_pwd_update(line, env);
  return (1);
}

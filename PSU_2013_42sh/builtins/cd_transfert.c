/*
** cd_transfert.c for kldfj in /home/da-sil_l/Dropbox/ALL/Dropbox/Clement
** 
** Made by da-sil_l
** Login   <da-sil_l@epitech.net>
** 
** Started on  Wed Mar 19 14:52:51 2014 da-sil_l
** Last update Thu May 22 20:07:20 2014 Clément
*/

#include <stdlib.h>
#include "mnsh.h"

int	cd_pwd_to_oldpwd(char *line, char ***env)
{
  int	i;
  char	**tab;

  if ((tab = malloc(5 * sizeof(char *))) == NULL)
    return (-1);
  i = 0;
  while ((*env)[i] != NULL && my_strncmp((*env)[i], "PWD=", 4) != 0)
    ++i;
  if ((*env)[i] == NULL)
    return (-1);
  tab[0] = my_strdup("setenv");
  tab[1] = my_strdup("OLDPWD");
  tab[2] = my_strdup(&(*env)[i][4]);
  tab[3] = my_strdup("1");
  tab[4] = NULL;
  my_setenv(tab, env);
  return (1);
}

int	cd_old_to_pwd(char ***env)
{
  int	i;
  char	**tab;
  char	*savepwd;
  char	*pwd;

  if ((tab = malloc(5 * sizeof(char *))) == NULL)
    return (-1);
  i = 0;
  savepwd = my_strdup(find_my(*env, "OLDPWD="));
  while ((*env)[i] != NULL && my_strncmp((*env)[i], "PWD=", 4) != 0)
    ++i;
  if ((*env)[i] == NULL)
    return (-1);
  pwd = my_strdup(&(*env)[i][4]);
  tab[0] = my_strdup("setenv");
  tab[1] = my_strdup("OLDPWD");
  tab[2] = my_strdup(pwd);
  tab[3] = my_strdup("1");
  tab[4] = NULL;
  my_setenv(tab, env);
  tab[1] = my_strdup("PWD");
  tab[2] = my_strdup(savepwd);
  my_setenv(tab, env);
  return (1);
}
